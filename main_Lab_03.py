"""@file main_Lab_03.py
Documentation for main_Lab_03.py

@brief  This file collects data and sends it to a PC
@detail This file waits for a start command from the front-end.  It then collects 
        voltage data from an analog button push and send the data back to  the
        PC.  When putting th file on the Nucleo, the file will be renamed to
        'main.py' so as to automatically run.
@author Brennen Irey
@date   January 26, 2021
"""


""" Initialize code needed to run the back-end data collection
"""
import array
import pyb
from pyb import UART

myuart          = UART(2)
adc             = pyb.ADC(pyb.Pin.board.A0)
timer           = pyb.Timer(2, freq = 500000)
buffer          = array.array('H', (0 for n in range(1000)))
user_command    = None
data            = []

button_press = False

def callback(pinSource):
    """ Sets button_press to True
    """
    global button_press
    button_press = True
    
extint = pyb.ExtInt(pyb.Pin.cpu.C13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_NONE, callback = callback)


""" Begin data collection code
"""

while True:
    """ While loop runs until the start command, 'g', is received from the PC
        front-end.
    """
    if myuart.any() != 0:
        user_command = myuart.readchar()
        if user_command == 103:
            myuart.write('data collection active')
            user_command = None
            break
        else:
            pass
    else:
        pass

while True:
    """ While loop runs until the user-button is pressed on the Nucleo.  This 
        begins data collection and data is stored to the buffer array.
    """
    if button_press == True:
        adc.read_timed(buffer,timer)
        for n in range(len(buffer)):
            myuart.write('{:}, {:}\n'.format(n, buffer[n]))
            #print('{:}, {:}'.format(n, buffer[n]))
        button_press = False
        break
    else:
        pass



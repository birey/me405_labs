'''
@file Lab4_main.py
@brief Main file that uses the mcp9808 module to write data into a .csv file
@details This program lives onboard the Nucleo Microcontroller along with the 
            mcp9808 module. They work in tandem to produce organized data located within
            a temperature data .csv file 'TempSensorData.csv' to then be transmitted over
            serial communication to a front end computer that will then plot data
            of ambient and internal temperatures of the air and the cpu over a duration
            of 8 hours, taking data once every minute.
@author Jacob Lindberg, Brennen Irey
@date Feburary 10, 2021
'''

from Lab4_mcp9808 import mcp9808
import pyb


## Manufacturer Slave Address of mcp9808
slave_address       = 0b0011001 # Connect A2 to GND, A1 to GND, A0 to power
## Manufacturer ID
ManfacID            = 0b01010100
## Manufacturer ID Register Address
ManfacRegAddr       = 0b00000110
## Temperature Register Address
TempReg             = 0b00000101
## Task Object for the mcp9808 temperature sensor
sensor_function     = mcp9808(slave_address, ManfacID, ManfacRegAddr, TempReg)
## Time Array
times               = []
## Temperature Data Array of Microcontroller
temp_data           = []
## Temperature Data Array of Ambient Air
sensor_data         = []
## ADCAll object with a resolution of 12 bits
adcall              = pyb.ADCAll(12, 0x70000)

if sensor_function.check() == True:
    i = 0
    for i in range(3):
        # Print data once every second
        sensor_function.myLED.high()
        pyb.delay(500)
        sensor_function.myLED.low()
        pyb.delay(500)
        i += 1
    ## Indexer used to calculated minutes of program elapsed
    adcall.read_core_vref()
    i = 0
   #with open('TempSensorData.csv', 'w') as file:
    while True:
        try:
            times.append(i)
            temp_data.append(round(adcall.read_core_temp(),1))
            sensor_data.append(round(sensor_function.celcius(),1))
            #file.write('{:},{:},{:}\r'.format(times[i], temp_data[i], sensor_data[i]))
            #file.write('{:},{:}\r'.format(times[i], sensor_data[i]))
            i += 1
            pyb.delay(60000)
        except KeyboardInterrupt:
            break
    with open('TempSensorData.csv', 'w') as file:
        for n in range(len(times)):
            file.write('{:},{:},{:}\r'.format(times[n], temp_data[n], sensor_data[n]))
        file.close()
else:
    print("Sensor Check Error! Check Pin Assignment...")

"""@file FSM_Lab_01.py
Documentation for/use of FSM_Lab_01.py

@brief  This file controls operations of a vending machine.
@detail This file uses an FSM to control a virtual vending machine with 4 drink choices.
        A drink can be selected at anytime and the current balance can be ejected at anytime.
@image html FSM_Diagram_Lab_01.jpg width = 1200px
@author Brennen Irey
@date   January 12, 2021
"""

"""Initialize code to run FSM (not the init state).  This code initializes the
program, not the FSM
"""
import keyboard


state = 0
pushed_key = None
balance = [0,0,0,0,0,0,0,0]
curr_balance = 0

def getChange(price, payment):
    """ Computes change
    """
    change = [0,0,0,0,0,0,0,0]                      # Initializes change list
    cent_values = [1,5,10,25,100,500,1000,2000]     # List with the cent values 
                                                    # of all payment denominations
    payment_cents = 0                               # Initializes variable
    
    for n in range(len(payment)):
        payment_cents += payment[n] * cent_values[n]
    
    change_cents = payment_cents - price
    
    if change_cents < 0:                # Checks to make sure payment is at least the price
        print("Insufficient payment")
        return False
    
    for n in range(len(payment)):       # Determines change denominations
    # Indexing at '7-n' allows the largest denomination to be calculated first 
    # without having to alter the list later
        change[7-n] = int(change_cents/cent_values[7-n]) 
        change_cents = change_cents % cent_values[7-n]
        
    #change = tuple(change)
    return change


def getBalance(balance):
    """ Computes balance in cents
    """
    cent_values = [1,5,10,25,100,500,1000,2000]     # List with the cent values 
                                                    # of all payment denominations
    balance_cents = 0                               # Initializes variable
    
    for n in range(len(balance)):
        balance_cents += balance[n] * cent_values[n]
    return balance_cents


def printWelcome():
    """Prints a Vendotron^TM^ welcome message with beverage prices
    """
    print("Treat yourself to a cold dronk:\nPress 'C' for Cuke ------- $1.00\nPress 'P' for Popsi ------ $1.20\nPress 'S' for Spryte ----- $0.85\nPress 'D' for Dr. Pupper - $1.10 ")
    pass


def on_keypress (thing):
    """ Callback which runs when the user presses a key.
    """
    global pushed_key

    pushed_key = thing.name

keyboard.on_press (on_keypress)


def updateScreen():
    """Clears Spyder REPL and updates the current balance
    """
    try:
        from IPython import get_ipython
        get_ipython().magic('clear')
        get_ipython().magic('reset -f')
    except:
        pass
    curr_balance = getBalance(balance)
    printWelcome()
    print("\nCurrent balance: " + str(curr_balance) + " cents\n")


while True:
    """while loop FSM implementation.  FSM will run until user presses CTRL+C
    """
    
    if state == 0:
        """Runs state 0 operations
        State 0 is an init state that initializes the FSM
        """
        
        printWelcome()
        state = 1       # Runs State 1 on the next iteration
    
    elif state == 1:
        """Runs state 1 operations
        State 1 waits for user interaction
        """
        
        
        try:
        # If a key has been pressed, check if it's a key we care about
            if pushed_key:
                if pushed_key == "0":
                    balance[0] += 1
                    updateScreen()
                    pushed_key = None
                    
                elif pushed_key == '1':
                    balance[1] += 1
                    updateScreen()
                    pushed_key = None
                    
                elif pushed_key == '2':
                    balance[2] += 1
                    updateScreen()
                    pushed_key = None
                    
                elif pushed_key == '3':
                    balance[3] += 1
                    updateScreen()
                    pushed_key = None
                    
                elif pushed_key == '4':
                    balance[4] += 1
                    updateScreen()
                    pushed_key = None
                    
                elif pushed_key == '5':
                    balance[5] += 1
                    updateScreen()
                    pushed_key = None
                    
                elif pushed_key == '6':
                    balance[6] += 1
                    updateScreen()
                    pushed_key = None
                    
                elif pushed_key == '7':
                    balance[7] += 1
                    updateScreen()
                    pushed_key = None
                    
                elif pushed_key == 'c':
                    state = 2
                elif pushed_key == 'p':
                    state = 2
                elif pushed_key == 's':
                    state = 2
                elif pushed_key == 'd':
                    state = 2
                elif pushed_key == 'e':
                    pushed_key = None
                    state = 3
                else:
                    print ("That is not a valid input")
                    pushed_key = None
    
            # If Control-C is pressed, this is sensed separately from the keyboard
            # module; it generates an exception, and we break out of the loop
        except KeyboardInterrupt:
            break
    
    elif state == 2:
        """Runs state 2 operations
        State 2 subtracts price from balance and vends if balance is sufficient
        """
        if pushed_key == 'c':
            if getChange(100, balance) == False:
                state = 1
            else:
                balance = getChange(100, balance)
                updateScreen()
                print("Vending Cuke.  Enjoy!")
                state = 3
        elif pushed_key == 'p':
            if getChange(120, balance) == False:
                state = 1
            else:
                balance = getChange(120, balance)
                updateScreen()
                print("Vending Popsi.  Enjoy!")
                state = 3
        elif pushed_key == 's':
            if getChange(85, balance) == False:
                state = 1
            else:
                balance = getChange(85, balance)
                updateScreen()
                print("Vending Spryte.  Enjoy!")
                state = 3
        elif pushed_key == 'd':
            if getChange(110, balance) == False:
                state = 1
            else:
                balance = getChange(110, balance)
                updateScreen()
                print("Vending Dr. Pupper.  Enjoy!")
                state = 3
        pushed_key = None
                
    elif state == 3:
        """Runs state 3 operations
        State 3 ejects the current balance using as few different denominations as possible
        """
        print("\nEjecting:\n{:} Pennies\n{:} Nickels\n{:} Dimes\n{:} Quarters\n{:} Dollar Bills\n{:} Five Dollar Bills\n{:} Ten Dollar Bills\n{:} Twenty Dollar Bills".format(balance[0],balance[1],balance[2],balance[3],balance[4],balance[5],balance[6],balance[7],))
        balance = [0,0,0,0,0,0,0,0]
        state = 1
        

"""@file Lab_07_ER_TFT080_1_Driver.py
Driver for the ER-TFT080-1 Resistive Touch Screen.

@brief  This code creates a touch screen driver object.
@detail This file accepts pin locations as inputs to initialize a touch screen
        object.  It also contains functions to retrieve position values and a 
        boolean value that shows if something is in contact with the screen.
@author Brennen Irey
@date   March 2, 2021
"""


import pyb
from pyb import Pin
from pyb import ADC
import utime


class ER_TFT080_1:
    """ @brief Driver for the ER-TFT080-1 resistive touch screen.  Allows the
               user to determine the x and y position of something on the screen.
    """
    
    def __init__(self, pin1, pin2, pin3, pin4):
        """ @brief Initializes the ER-TFT080-1 object and creates copies of all 
                   input parameters.
            @param pin1 The pin on the Nucleo corresponding to the Xm output
                        of the resistive touch screen.
            @param pin2 The pin on the Nucleo corresponding to the Yp output
                        of the resistive touch screen.
            @param pin3 The pin on the Nucleo corresponding to the Xp output
                        of the resistive touch screen.
            @param pin4 The pin on the Nucleo corresponding to the Ym output
                        of the resistive touch screen.
        """
        
        self.pin1 = pin1        
        self.pin2 = pin2        
        self.pin3 = pin3        
        self.pin4 = pin4
    
        self.pin_xm = Pin(pin1) 
        self.pin_yp = Pin(pin2)
        self.pin_xp = Pin(pin3)
        self.pin_ym = Pin(pin4)
        
    def x_scan(self):
        """ @brief Determines the raw x position
        """
        self.pin_xm.init(mode = Pin.OUT_PP, value = 0)
        self.pin_xp.init(mode = Pin.OUT_PP, value = 1)
        #ADC_ym = ADC(self.pin4)
        self.pin_ym.init(mode = Pin.ANALOG)
        ADC_ym = ADC(self.pin_ym)
        x_pos = ADC_ym.read()
        return x_pos
    
    def y_scan(self):
        """ @brief Determines the raw y position
        """
        self.pin_ym.init(mode = Pin.OUT_PP, value = 0)
        self.pin_yp.init(mode = Pin.OUT_PP, value = 1)
        #ADC_xm = ADC(self.pin1)
        self.pin_xm.init(mode = Pin.ANALOG)
        ADC_xm = ADC(self.pin_xm)
        y_pos = ADC_xm.read()
        return y_pos
    
    def z_scan(self):
        """ @brief Determines the z position (The z position refers to whether or
                   not anything is in contact with the resistive touch screen)
        """
        self.pin_xm.init(mode = Pin.OUT_PP, value = 0)
        self.pin_yp.init(mode = Pin.OUT_PP, value = 1)
        #ADC_xp = ADC(self.pin3)
        self.pin_xp.init(mode = Pin.ANALOG)
        ADC_xp = ADC(self.pin_xp)
        contact = False
        if ADC_xp.read() > 0:
            contact = True
        return contact

    def all_scan(self):
        """ @brief Calls all positional methods to determine overall position
        """
        start_time = utime.ticks_us()
        print(self.z_scan())
        print(self.y_scan())
        print(self.x_scan())
        elapsed_time = utime.ticks_us() - start_time
        print(elapsed_time)
        
        
"""@file Rxn_Time_Lab_02.py
Documentation for/use of Rxn_Time_Lab_02.py

@brief  This file demonstrates the function of interrupts and callback functions.
@detail This file uses an external interrupt in the form of a button to test 
        the reaction time of the user.  The user attempts to press a button as 
        soon as an LED is turned on, causing an interrupt. 
@author Brennen Irey
@date   January 23, 2021
"""

"""Initialize code to run the reaction time test
"""
import urandom
import pyb
import utime


rxn_times = []      # Initializes array storing reaction times

rxn_time = 0        # Initializes reaction time storage variable for use in the callback function

rxn_time_avg = 0    # Initializes average reaction time value


def callback(line):
    """ Updates the value of the rxn_time variable with the time elapsed in 
        microseconds since the LED was turned on
    """
    global rxn_time 
    rxn_time = utime.ticks.ms()

    
extint = pyb.ExtInt(pyb.Pin.board.PC13, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, callback)

timer = pyb.Timer(2, prescaler = 79, period = 0x3fffffff)

userLED = pyb.Pin(pyb.Pin.board.PA5, mode=pyb.Pin.OUT_PP)


while True:
    try:
        delay = urandom.randrange(2000, 3000) # Chooses a random delay time in microseconds between 2-3 seconds
        utime.sleep_ms(delay)
        timer.counter(0)
        userLED.high()
        rxn_times.append(rxn_time)
    
    except KeyboardInterrupt:
        for n in rxn_times:
            rxn_time_avg += rxn_times[n]
        rxn_time_avg = int(rxn_time_avg/len(rxn_times))
        print('The average reaction time was {:} milliseconds.'.format(rxn_time_avg))
        break
        
     
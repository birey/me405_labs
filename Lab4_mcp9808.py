'''
@file Lab4_mcp9808.py
@brief Driver for the mcp9808 I2C temperature sensor
@details This program is a driver for a mcp9808 temperature sensor.  It allows 
         users to check that the I2C connection is correct as well as read the
         temperature in units of celsius or farenheit.
@author Jacob Lindberg, Brennen Irey
@date Feburary 10, 2021
'''

from pyb import I2C
import pyb


class  mcp9808:
    """ @brief Driver for the mcp9808 sensor.  Allows checking of the I2C connection
               and reading of the temperature in celsius  and/or farenheit.
    """
    def __init__(self, address, ManfacID, ManfacRegAddr, TempReg):
        """ @biref Initializes the mcp9808 object and creates object copies of
                   all input parameters.
            @param address The address of the mcp9808 temperature sensor.
            @param ManfacID The manufacturer assigned ID register value.
            @param ManfacRegAddr The ID register address.
            @param TempReg The temperature resgister address
            """
            
        self.mcp9808        = I2C(1, I2C.MASTER)
        self.address        = address
        self.ManfacID       = ManfacID
        self.ManfacRegAddr  = ManfacRegAddr
        self.TempReg        = TempReg
        self.SDA            = pyb.Pin.board.PB9
        self.SCL            = pyb.Pin.board.PB8
        self.myLED          = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)    
        
    def check(self):
        """ @brief Checks that the I2C connections is active and correct.
        """
        ID = bytearray(self.mcp9808.mem_read(2, addr = self.address, memaddr = self.ManfacRegAddr))
        if ID[1] == self.ManfacID:
            return True
        else:
            return False
        
    def celcius(self):
        """ @brief Reads the ambient temperature in degrees celsius.
        """
        data = self.mcp9808.mem_read(2, addr = self.address, memaddr = self.TempReg)
        [x, data1] = bin(data[0]).split('b')
        [x, data2] = bin(data[1]).split('b')
        byte1_len = len(data1) - 4
        byte2_len = len(data2)
        data_null = 0
        n = 0
        for n in range(byte2_len):
            data_null += int(data2[n])*(2**((byte2_len-5) - n))
        n = 0
        for n in range(byte1_len):
            data_null += int(data1[n+4])*(2**((byte1_len+3) + n))
        if data1[3] == 1:
            data_null = -data_null
            return data_null
        else:
            return data_null
        
    def fehrenheit(self):
        """ @brief Reads the ambient temperature in farenheit.
        """
        data_null = self.celcius()
        data_null = (9/5)*data_null + 32
        return data_null
    
            
if __name__ == "__main__":
   slave_address = 0b0011001 # Connect A2 to GND, A1 to GND, A0 to power
   sensor_function = I2C(slave_address)
   if sensor_function.check() == True:
       i = 0
       for i in range(3):
           # Print data once every second
           sensor_function.myLED.high()
           pyb.delay(500)
           sensor_function.myLED.low()
           pyb.delay(500)
           print(sensor_function.celcius())
           i += 1
       



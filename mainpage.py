##@file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
# @mainpage
#
# @section sec_intro Introduction
#  This portfolio contains documentation for all ME 405 lab work.  All labs
#  are contained in their own page.
#
# @page page_lab01 Lab 01
#
# @section page_lab01_src Source Code Access
#
# The source code for the Vendotron^TM^ can be found here:
#
# https://bitbucket.org/birey/me405_labs/src/master/FSM_Lab_01.py
#
# @section page_lab01_doc Documentation
#
# The documentation for the Vendotron^TM^ code is located at @ref FSM_Lab_01.py
#
# The FSM Diagram is shown below:
# @image html FSM_Diagram_Lab_01.png width = 1200px
#
# @page page_lab02 Lab 02
#
# @section page_lab02_src Source Code Access
#
# The source code for the Reaction Time Test can be found here:
#
# https://bitbucket.org/birey/me405_labs/src/master/Rxn_Time_Lab_02.py
#
# @section page_lab02_doc Documentation
#
# The documentation for the Reaction Time Test code is located at @ref Rxn_Time_Lab_02.py
#
# @page page_lab03 Lab 03
#
# @section page_lab03_src Source Code Access
#
# The source code for the UI Front-End can be found here:
#
# https://bitbucket.org/birey/me405_labs/src/master/UI_Front_End_Lab_03.py
#
# The source code for the main file can be found here:
#
# https://bitbucket.org/birey/me405_labs/src/master/main_Lab_03.py
#
# @section page_lab03_doc Documentation
#
# The documentation for the UI Front-End code is located at @ref UI_Front_End_Lab_03.py
#
# The documentation for the main file is located at @ref main_Lab_03.py
#
# @page page_lab04 Lab 04
#
# @section page_lab04_src Source Code Access
#
# The source code for the MCP9808 Temperature Sensor driver can be found here:
#
# https://bitbucket.org/jacoblindberg20/lab4_teamrepository/src/master/Lab4_mcp9808.py
#
# The source code for the main file can be found here:
#
# https://bitbucket.org/jacoblindberg20/lab4_teamrepository/src/master/Lab4_Main.py
#
# @section page_lab04_doc Documentation
#
# The documentation for the MCP9808 driver code is located at @ref Lab4_mcp9808.py
#
# The documentation for the main file is located at @ref Lab4_main.py
#
# @section page_lab04_data Data
#
# During this lab, ambient temperature and the core temperature of the Nucleo 
# were periodically recorded.  The resulting data was plotted and the results 
# are pictured below:
# @image html Core_temp_vs_time_Lab_04.png width = 1200px
# @image html Ambient_temp_vs_time_Lab_04.png width = 1200px
#
# @page page_lab05 Lab 05
#
# @section page_lab05_description Description
#
# During this lab, our team derived equations of motion needed to relate the position
# a ball on a blancing board to the angle of a motor attatched to said balancing board.
# The actual system can rotate around two axes but we simplified the model to 
# only account for rotation about one axis and ball movement in one direction.
#
# The full balancing board is pictured below:
# @image html ME405_Platform.jpg width=700px height=600px
#
# @section page_lab05_calcs Calculations
#
# Pictured below are the hand calculations performed by our team.
#
# Platform & Ball Kinematics:
# @image html ME405_Lab5_Kinematics_1.jpg width=800px height=1035px
# @image html ME405_Lab5_Kinematics_2.jpg width=800px height=1035px
# @image html ME405_Lab5_Kinematics_3.jpg width=800px height=1035px
#
# Platform & Ball Kinetics:
# @image html ME405_Lab5_Kinetics_1.jpg width=800px height=1035px
# @image html ME405_Lab5_Kinetics_2.jpg width=800px height=1035px
# @image html ME405_Lab5_Kinetics_3.jpg width=800px height=1035px
# @image html ME405_Lab5_Kinetics_4.jpg width=800px height=1035px
# @image html ME405_Lab5_Kinetics_5.jpg width=800px height=1035px
#
# Ball Kinetics Revisited:
# @image html ME405_Lab5_Kinetics_6.jpg width=800px height=1035px
# @image html ME405_Lab5_Kinetics_7.jpg width=800px height=1035px
#
# @page page_lab06 Lab 06
# 
# @section page_lab06_description Description
#
# The goal of this lab was to adapt the equations derived in @ref page_lab05 
# into a usable state-space matrix form in order to model the system.  The 
# equations first needed to be linearized using the Jacobian method before they 
# could be arranged into state-space form.
#
# @page page_lab07 Lab 07
#
# @section page_lab07_src Source Code Access
#
# The source code for the ER-TFT080-1 resistive touch screen driver can be found here:
#
# https://bitbucket.org/birey/me405_labs/src/master/Lab_07_ER_TFT080_1_Driver.py
#
# The source code for the main file can be found here:
# 
# https://bitbucket.org/birey/me405_labs/src/master/Lab_07_main.py
#
# @section page_lab07_doc Documentation
#
# The documentation for the ER-TFT080-1 driver code is located at @ref Lab_07_ER_TFT080_1_Driver.py
#
# The documentation for the main file is located at @ref Lab_07_main.py
#
# @page page_lab08 Lab 08
#
# @section page_lab08_src Source Code Access
#
# The source code for the motor driver can be found here:
#
# https://bitbucket.org/cameronngai/me_405/src/master/MotorDriver.py
#
# The source code for the encoder driver can be found here:
#
# https://bitbucket.org/cameronngai/me_405/src/master/Encoder.py
#
# @section page_lab08_doc Documentation
#
# The documentation for the motor driver is located at @ref MotorDriver.py
#
# The documentation for the encoder driver is located at @ref Encoder.py
#
# @page page_lab09 Lab 09
#
# @section page_lab09_src Source Code Access
#
# The source code for the touchscreen driver can be found here:
#
# https://bitbucket.org/cameronngai/me_405/src/master/touch.py
#
# The source code for the main file (termproject.py) can be found here:
#
# https://bitbucket.org/cameronngai/me_405/src/master/termproject.py 
#
# @section page_lab09_doc Documentation
#
# The documentation for the touchscreen driver is located at @ref touch.py
#
# The documentation for the main file is located at @ref termproject






"""@file UI_Front_End_Lab_03.py
Documentation for UI_Front_End_Lab_03.py

@brief  This file facilitates communication with a Nucleo MCU
@detail This file waits for the letter 'G' to be pressed.  When pressed, it is
        sent to the Nucleo and used to start data collection.  Once the data
        collection is complete, it is sent back to the PC and plotted/formatted.
@author Brennen Irey
@date   January 26, 2021
"""


""" Initialize code needed to run the UI front-end
"""

# Import required modules
import keyboard
import serial
from matplotlib import pyplot as pyp
import numpy as np


# Initialize variables
pushed_key      = None
user_command    = None
timeout_count   = 0
data_line_s     = 0
data_line_l     = []
data_csv        = []
voltage         = []
times           = []

# Initialize serial port
ser = serial.Serial(port = 'COM4', baudrate = 115273, timeout = 1)

# Define keypress interrupt callback
def on_keypress (key):
    """ Callback which runs when the user presses a key.
    """
    global pushed_key

    pushed_key = key.name

keyboard.on_press(on_keypress)


""" Begin UI front-end code
"""
print('first while')
while True:
    """ While loop runs until the 'G' key is pressed on the keyboard
    """
    if pushed_key == 'G' or pushed_key == 'g':
        user_command = 'g' 
        ser.write(user_command.encode('ascii'))
        pushed_key = None
        user_command = None

        print('command snet')
        break
    else:
        pass
   
print('second while')
ser.readline()
while True:
    """ While loop appends collected data to a list for CSV export as well as two 
        other lists for time and voltage values.  It runs until 100 loops have 
        elapsed since the last serial readline.
    """
    if ser.in_waiting != 0:
        timeout_count = 0 # Resets timeout counter
        data_line_s = ser.readline().decode('ascii')
        data_csv.append(data_line_s.strip())
        data_line_l = data_line_s.strip().split(',')
        times.append(float(data_line_l[0]))
        voltage.append(float(data_line_l[1])*0.00081) # 0.00081 is the conversion to volts
        if float(data_line_l[1]) >= 4050:
            break
        else:
            pass
        pass
    # elif timeout_count > 1000000:
    #     print('collection finished')
    #     ser.close()
    #     break
    else:
        timeout_count += 1
        pass

# The following code plots the collected data
pyp.plot(times,voltage,"g--")
pyp.xlabel('Time')
pyp.ylabel('Voltage')
pyp.show()

# The following code saves the collected data as a CSV file
np.savetxt("Lab_03_data.csv", data_csv, delimiter =", ", fmt ='% s') 
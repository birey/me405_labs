"""@file Lab_07_main.py
"""

import pyb
import utime
from Lab_07_ER_TFT080_1_Driver import ER_TFT080_1

tch = ER_TFT080_1('A0', 'A6', 'A1', 'A7')

while True:
    print(tch.all_scan())
    utime.sleep_ms(500)
    